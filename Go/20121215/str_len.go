package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	var en string = "golang"
	var jp string = "Go言語"

	fmt.Println(en, " length(byte):", len(en), " Count:", utf8.RuneCountInString(en))
	fmt.Println(jp, " length(byte):", len(jp), " Count:", utf8.RuneCountInString(jp))
}
