package main

import (
	"flag"
	"fmt"
	"log"
)

// エラー型定義
type ArithError struct {
	message string
}

// エラー内容
func (err ArithError) Error() string {
	return err.message
}

// エントリーポイント
func main() {
	// コマンドパラメータ定義
	flag1 := flag.Int("num1", 12, "整数")
	flag2 := flag.Int("num2", 567, "整数")
	// コマンドパラメータの解析
	flag.Parse()

	// コマンドパラメータから値を取得
	var num1 int32 = int32(*flag1)
	var num2 int32 = int32(*flag2)
	fmt.Printf("int32:%d+%d=%d\n", num1, num2, num1+num2)

	// 文字列型に変換する。数値は文字コードとして扱われる
	strNum1 := string(num1)
	strNum2 := string(num2)
	fmt.Printf("string:%d+%d=%s\n", num1, num2, strNum1+strNum2)

	// 数値を結合する
	cmbNum, err := combine(num1, num2)

	if err == nil {
		fmt.Printf("combine:%d+%d=%d\n", num1, num2, cmbNum)
	} else {
		fmt.Println(err)
	}
}

// 結合関数
func combine(num1 int32, num2 int32) (int32, error) {
	cnt := 0
	num := num2
	for num > 0 {
		num /= 10
		cnt++
	}

	log.Println("cnt=", cnt)

	if false == validatedRangeShift(num1, cnt) {
		return 0, ArithError{"overflow exception"}
	}

	for i := 0; i < cnt; i++ {
		num1 *= 10
	}
	return num1 + num2, nil
}

// 桁移動の有効チェック関数
func validatedRangeShift(num int32, shiftCnt int) bool {
	// int32の最大値は2147483647、最小値は-2147483648なので、オーフローしないかチェックする
	maxValue := int32(2147483647)
	minValue := int32(-2147483648)

	for i := 0; i < shiftCnt; i++ {
		maxValue /= 10
		minValue /= 10
	}

	if num >= maxValue || num <= minValue {
		return false
	}

	return true
}
