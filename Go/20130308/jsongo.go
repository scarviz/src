package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type Message struct {
	Id   int64
	Data string
}

func main() {
	mes := Message{1, "JSONTest"}

	// JSONにエンコーディングする
	bdata := EncodingJSON(mes)
	// byteスライスを出力する
	os.Stdout.Write(bdata)
	fmt.Println("")

	// JSONをデコーディングする
	dmes := DecodingJSON(bdata)
	// 構造体を出力する
	fmt.Println(dmes)
}

/*
JSONにエンコーディングする
*/
func EncodingJSON(mes Message) []byte {
	bdata, err := json.Marshal(mes)

	if err != nil {
		fmt.Println(err)
		return nil
	}

	return bdata
}

/*
JSONにデコーディングする
*/
func DecodingJSON(bdata []byte) Message {
	var mes Message

	err := json.Unmarshal(bdata, &mes)

	if err != nil {
		fmt.Println(err)
		return Message{-1, "err"}
	}

	return mes
}
