package main

import (
	"fmt"
	"github.com/ziutek/mymysql/mysql"
	_ "github.com/ziutek/mymysql/thrsafe"
	"os"
)

/*
エラーを検出する
*/
func checkError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

/*
エラーを検出し、実行結果を返す
*/
func checkedResult(rows []mysql.Row, res mysql.Result, err error) ([]mysql.Row, mysql.Result) {
	checkError(err)
	return rows, res
}

func main() {
	proto := "tcp"
	addr := "127.0.0.1:3306"
	dbname := "sampledb"
	table := "sample_table"
	
	var user string
	var pass string
	fmt.Println("ユーザ名を入力してください")
	fmt.Scanln(&user)
	fmt.Println("パスワードを入力してください")
	fmt.Scanln(&pass)

	// 接続情報を作成し、DBに接続する
	db := mysql.New(proto, "", addr, user, pass, dbname)
	checkError(db.Connect())
	fmt.Printf("%sに接続しました\n", dbname)

	// 遅延実行でDBを閉じる
	defer func() {
		checkError(db.Close())
		fmt.Println("接続を閉じました")
	}()

	executesql(db, table)
}

/*
SQL文を実行する
*/
func executesql(db mysql.Conn, table string) {
	num := 0
	isexit := false
	for !isexit {
		var selectNum int
		fmt.Println("実行する対象の番号を入力してください")
		fmt.Println("1.insert 2.select 3.update 4.delete  (otherNum cancel)")
		fmt.Scanln(&selectNum)

		switch selectNum {
		case 1:
			execinsert(db, table, num)
			num++
		case 2:
			execselect(db, table)
		case 3:
			execupdate(db, table)
		case 4:
			execdelete(db, table)
		default:
			isexit = true
		}
	}
}

/*
INSERT文を実行する
*/
func execinsert(db mysql.Conn, table string, num int) {
	// INSERT文作成
	sqlstr := "INSERT INTO " + table + " (name) VALUES ('test_%d')"
	// Query実行(パラメータは可変)
	checkedResult(db.Query(sqlstr, num))
	fmt.Println("insertを実行しました")
}

/*
SELECT文を実行する
*/
func execselect(db mysql.Conn, table string) {
	// SELECT文作成
	sqlstr := "SELECT * FROM " + table
	// Query実行
	rows, res := checkedResult(db.Query(sqlstr))

	// カラムのマッピング
	id := res.Map("id")
	name := res.Map("name")

	fmt.Println("| id | name |")
	for _, row := range rows {
		// 結果を1行ずつ表示する
		fmt.Printf("| %d | %s |\n", row.Int(id), row.Str(name))
	}
}

/*
UPDATE文を実行する
*/
func execupdate(db mysql.Conn, table string) {
	// トランザクション開始
	tr, err := db.Begin()
	checkError(err)

	// UPDATE文作成
	sqlstr := "UPDATE " + table + " SET name='test'"
	// UPDATE実行
	_, err = tr.Start(sqlstr)

	if err != nil {
		// UPDATE中に失敗したらロールバックする
		tr.Rollback()
		fmt.Println("Rollbackしました")
	} else {
		// 問題なくUPDATEが完了していればコミットする
		tr.Commit()
		fmt.Println("updateをCommitしました")
	}
}

/*
DELETE文を実行する
*/
func execdelete(db mysql.Conn, table string) {
	// DELETE文作成
	sqlstr := "DELETE FROM " + table + " WHERE name='test'"
	// Query実行
	checkedResult(db.Query(sqlstr))
	fmt.Println("deleteを実行しました")
}
