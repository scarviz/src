package main

import "io/ioutil"
import "fmt"
import "flag"

func main(){
    flag.Parse() // パラメータリストを調べてflagに設定

    switch{
        case flag.NArg() == 0:
            fmt.Println("引数が足りません。\"ファイルパス\"を指定してください")
            return
        case flag.NArg() > 1:
            fmt.Println("引数が多過ぎます。\"ファイルパス\"のみ指定してください")
            return
    }
    
    image,err := ioutil.ReadFile(flag.Arg(0))
    if err != nil{
        fmt.Println("読み込みに失敗しました")
        return
    }

    werr := ioutil.WriteFile(flag.Arg(0)+"_out.jpg", image, 0777)
    if werr != nil{
        fmt.Println("書き込みに失敗しました")
        return
    }else{
        fmt.Println("書き込みに成功しました")
    }
}
