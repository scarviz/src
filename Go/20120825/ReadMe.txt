画像ファイルを読み書きするプログラムです。
コマンドラインで読み込む画像ファイルを指定してください。
読み込んだ画像ファイルは、ファイル名に「_out.jpg」をつけて、
同ディレクトリにそのまま出力されます。

ex.) samplepicディレクトリのgo_gopher.jpgを読み込む場合
　go run imageProcGo.go ./samplepic/go_gopher.jpg
