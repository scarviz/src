package main

import (
	"fmt"
	"github.com/visualfc/go-ui/ui"
	"io/ioutil"
)

// スレッド終了時用のチャネル
var exit = make(chan bool)

/*
* エントリーポイント
*/
func main() {	
	fmt.Println("ImageProc start")
	
	// uiのメイン関数を実行する
	ui.Main(func() {
		// main_ui関数を非同期実行
		go main_ui()
		// ui開始
		// (この関数でフォームが閉じられるまでループする)
		ui.Run()
		// main_ui関数の終了待ちをする(同期をとる)
		exit <- true
	})
}

/*
* 画面を形成するメインとなる関数
*/
func main_ui(){
	// ウィジェット(C＃とかでいうフォーム)準備
	w := ui.NewWidget()
	// ウィジェットタイトル設定
	w.SetWindowTitle("ImageProc")
	// ウィジェットサイズ設定
	w.SetSizev(400, 200)
	
	// 関数がリターンする時にウィジェットをクローズして、
	// リソースを開放するとスケジュールしておく
	defer w.Close()
	
	// ウィジェットのレイアウト定義
	// 縦レイアウト
	vbox := ui.NewVBoxLayout()
	// 横レイアウト
	hbox := ui.NewHBoxLayout()

	// メニューバー定義
	menubar := ui.NewMenuBar()
	// "File"という項目を設定
	menu := ui.NewMenuWithTitle("&File")
	// メニューバーに追加
	menubar.AddMenu(menu)
	
	// アクション定義
	act := ui.NewAction()
	// "Quit"という項目を設定
	act.SetText("&Quit")
	// "Quit"のアクション設定
	act.OnTriggered(func(bool) {
		ui.Exit(0)
	})
	// アクション定義。"Test1"という項目を設定
	act2 := ui.NewAction()
	act2.SetText("&Test1")
	// アクション定義。"Test2"という項目を設定
	act3 := ui.NewAction()
	act3.SetText("&Test2")
	
	// "File"項目に追加。1つ目と2つ目の間にセパレータをいれている
	menu.AddAction(act)
	menu.AddSeparator()
	menu.AddAction(act2)
	menu.AddAction(act3)
	
	// 縦レイアウトにメニューを加える
	vbox.SetMenuBar(menubar)
	
	// テキストボックス定義し、横レイアウトに設定する
	// ed.SetText("hoge") と書けば"hoge"がボックス内に表示される
	ed := ui.NewLineEdit()
	hbox.AddWidget(ed)
	
	// ボタン定義
	btn := ui.NewButton()
	// ボタンのテキスト設定
	btn.SetText("画像表示")
	// クリック時イベント定義
	btn.OnClicked(func() {
		// テキストボックスに入力した画像パスを渡して画像を表示する
		ShowImage(vbox, ed.Text())
	})
	
	// 横レイアウトにボタンを加える
	hbox.AddWidget(btn)
	
	// 縦レイアウトに横レイアウトを設定する
	vbox.AddLayout(hbox)
	w.SetLayout(vbox)
	
	// ウィジェットを表示状態にする
	w.SetVisible(true)
	
	// main_ui終了通知
	// (ウィジェットが閉じられて、Run関数のループを抜けて、同期をとるまで待つ)
	<-exit
}

/*
* 画像を表示する
*/
func ShowImage(vbox *ui.VBoxLayout, path string){
	// 画像ファイルを読み込む
	buf, err := ioutil.ReadFile(path)
	// 画像読み込みに失敗していない場合
	if err == nil {
		// 画像を縦レイアウトに設定する。ここの画像はPNGでないとだめみたい
		pixmap := ui.NewPixmapWithData(buf)
		vbox.AddWidget(ui.NewLabelWithPixmap(pixmap))
	}else{
		fmt.Println("No Image")
	}
}